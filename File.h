#ifndef FILE_H
#define FILE_H

#include <fstream>
using std::ifstream;
#include <sstream>
using std::stringstream;
#include <string>
using std::string;

class File: public ifstream {
    public:
        explicit File(string filename): ifstream(filename.c_str()) {
        }

        string read_all() {
            stringstream buffer;
            buffer << rdbuf();
            return buffer.str();
        }
};

#endif
