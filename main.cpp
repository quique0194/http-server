#include "./HttpServer.h"

int main() {
    HttpServer server(9000, "srv");
    server.run();
    return 0;
}
