#ifndef UTILS_H
#define UTILS_H

#include <cstring>

#include <vector>
using std::vector;
#include <string>
using std::string;
#include <sstream>
using std::stringstream;
#include <map>
using std::map;

#include "rapidxml.hpp"
using namespace rapidxml;


vector<string> split(const string& orig, char delim) {
    vector<string> ret;
    ret.push_back("");
    for (int i = 0; i < orig.size(); ++i) {
        if (orig[i] == delim) {
            ret.push_back("");
        } else {
            ret[ret.size()-1] += orig[i];
        }
    }
    return ret;
}

int strtoi(const string& str) {
    return atoi(str.c_str());
}

string itostr(int i) {
    stringstream ss;
    ss << i;
    return ss.str();
}

string get_mime_type(const string& extension) {
    /*
        - key: extension
        - value: mime_type
    */
    map<string, string> mime;
    mime["css"] = "text/css";
    mime["js"] = "application/x-javascript";
    mime["html"] = "text/html";
    return mime[extension];
}

string detect_content_type(const string& filename) {
    vector<string> list = split(filename, '.');
    string extension = list[list.size()-1];
    return get_mime_type(extension);
}


vector<xml_node<>*> find_nodes(xml_node<>* parent, const char* name) {
    vector<xml_node<>*> ret;
    if (parent != 0) {
        if (strcmp(parent->name(), name) == 0) {
            ret.push_back(parent);
        }
        for (xml_node<>* it = parent->first_node(); it != 0; it = it->next_sibling()) {
            vector<xml_node<>*> tmp = find_nodes(it, name);
            ret.insert(ret.end(), tmp.begin(), tmp.end());
        }
    }
    return ret;
}

vector<string> get_html_dependencies(const string& html) {
    char* cstr = new char[html.size() + 1];
    strcpy (cstr, html.c_str());

    xml_document<> doc;
    doc.parse<0>(cstr);

    vector<string> ret;

    vector<xml_node<>*> nodes = find_nodes(&doc, "link");
    for (auto node : nodes) {
        if (node->first_attribute("href") != 0) {
            ret.push_back(node->first_attribute("href")->value());
        }
    }
    nodes = find_nodes(&doc, "script");
    for (auto node : nodes) {
        if (node->first_attribute("src") != 0) {
            ret.push_back(node->first_attribute("src")->value());
        }
    }
    return ret;
}

#endif
