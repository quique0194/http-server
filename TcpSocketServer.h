#ifndef TCPSOCKETSERVER_H
#define TCPSOCKETSERVER_H

#include <sys/types.h>      // socket
#include <sys/socket.h>     // socket
#include <arpa/inet.h>      // htons, inet_pton

#include <string.h>         // memset
#include <unistd.h>         // read, write
#include <stdio.h>          // perror
#include <stdlib.h>         // exit

#include <thread>
using std::thread;

#include "./TcpSocketConnection.h"


class TcpSocketServerBase {
    /*
        Here we have only basic functions:
        - listen to port
        - close listening
        - accept connections
    */
    public:
        TcpSocketServerBase() {
            socket_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

            if (socket_fd == -1) {
                perror("Socket creation failed");
                exit(EXIT_FAILURE);
            }
        }

        void listen(int port) {
            memset(&stSockAddr, 0, sizeof(struct sockaddr_in));

            stSockAddr.sin_family = AF_INET;
            stSockAddr.sin_port = htons(port);
            stSockAddr.sin_addr.s_addr = INADDR_ANY;

            if (-1 == bind(socket_fd, (const struct sockaddr *)&stSockAddr,
                           sizeof(struct sockaddr_in))) {
                close();
                perror("Binding failed");
                exit(EXIT_FAILURE);
            }

            if (-1 == ::listen(socket_fd, 10)) {
                close();
                perror("Listen failed");
                exit(EXIT_FAILURE);
            }
        }

        void close() {
            shutdown(socket_fd, SHUT_RDWR);
            ::close(socket_fd);
        }

        TcpSocketConnection accept() {
            int connection_fd = ::accept(socket_fd, NULL, NULL);
            if (0 > connection_fd) {
                close();
                perror("Accept connection failed");
                exit(EXIT_FAILURE);
            }
            return TcpSocketConnection(connection_fd);
        }

    private:
        struct sockaddr_in stSockAddr;
        int socket_fd;
};

class TcpSocketServer: public TcpSocketServerBase {
    public:
        explicit TcpSocketServer(int _port): TcpSocketServerBase(),
                                             port(_port) {
        }

        void run() {
            listen(port);
            thread accept_conns_thread(&TcpSocketServer::accept_conns, this);
            accept_conns_thread.detach();
            printf("Press a key to stop server...\n");
            getchar();
            stop();
        }

    protected:
        virtual void handle_conn(TcpSocketConnection conn) {
            /* 
                Override this method.
                - Don't forget to close conn in the end or
                - Call parent function
            */
            conn.close();
        }

    private:
        int port;

        void stop() {
            close();
        }

        void accept_conns() {
            while (1) {
                TcpSocketConnection conn = accept();
                thread handle_conn_thread(&TcpSocketServer::handle_conn,
                                          this, conn);
                handle_conn_thread.detach();
            }
        }
};

#endif
