#ifndef REQUEST_H
#define REQUEST_H

#include <iostream>
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <map>
using std::map;
#include <vector>
using std::vector;

#include "./utils.h"


class Request {
    public:
        string method;
        string path;
        string version;
        map<string, string> headers;

        void parse_first_line(const string& line) {
            vector<string> args = split(line, ' ');
            method = args[0];
            path = args[1];
            version = args[2];
        }

        void parse_header_line(const string& line) {
            vector<string> args = split(line, ':');
            string key = args[0];
            string val = args[1];
            headers[key] = val;
        }

        void info() {
            cout << "Request: ";
            cout << method << " " << path << " " << version << endl;
        }
};

#endif
