#ifndef RESPONSE_H
#define RESPONSE_H

#include <string>
using std::string;
#include <map>
using std::map;

#include "./Request.h"
#include "./utils.h"

class Response {
    public:
        string version;
        string status;
        string phrase;
        map<string, string> headers;
        string body;

        string to_string() {
            string ret = version + " " + status + " " + phrase + "\r\n";
            map<string, string>::iterator it;

            headers["Content-Length"] = itostr(body.size());
            for (it = headers.begin(); it != headers.end(); ++it) {
                ret += it->first + ": " + it->second + "\r\n";
            }
            ret += "\r\n";
            ret += body;
            return ret;
        }
};

Response not_found(Request request) {
    Response response;
    response.body = "<h1>404: Not found</h1>";
    response.version = request.version;
    response.status = "404";
    response.phrase = "Not Found";
    response.headers["Content-Type"] = "text/html";
    return response;
}

Response not_supported_version(Request request) {
    Response response;
    response.body = "<h1>505: Not supported http version</h1>";
    response.version = request.version;
    response.status = "505";
    response.phrase = "Not supported http version";
    response.headers["Content-Type"] = "text/html";
    return response;
}

#endif
