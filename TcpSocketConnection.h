#ifndef TCPSOCKETCONN_H
#define TCPSOCKETCONN_H


#include <string>
#include <mutex>
using std::string;
using std::mutex;
using std::lock_guard;


#define CHUNK_SIZE 100     // How much chars are read from socket at once


class TcpSocketConnection {
    public:
        explicit TcpSocketConnection(int _connection_fd = 0):
                connection_fd(_connection_fd), write_mtx(new mutex()) {
        }

        ~TcpSocketConnection() {
        }

        string read(int n) {
            /*
                Read n bytes of data
            */
            string ret;
            for (int i = 0; i < n / CHUNK_SIZE; ++i) {
                ret += read_chunk();
            }
            for (int i = 0; i < n % CHUNK_SIZE; ++i) {
                ret += read_byte();
            }
            return ret;
        }

        string read_line() {
            /*
                Doesn't include '\n'
            */
            string ret;
            char next = read_byte();
            while (next != '\n') {
                ret += next;
                next = read_byte();
            }
            if (ret.back() == '\r') {
                return string(ret.begin(), ret.end()-1);
            } else {
                return ret;
            }
        }

        char read_byte() {
            char ret;
            ::read(connection_fd, &ret, 1);
            return ret;
        }

        void write(const string& msg) {
            lock_guard<mutex> guard(*write_mtx);
            ::write(connection_fd, msg.c_str(), msg.size());
        }

        void close() {
            if (connection_fd != 0 ) {
                shutdown(connection_fd, SHUT_RDWR);
                ::close(connection_fd);
                connection_fd = 0;
                delete write_mtx;
            }
        }

        bool is_connected() {
            return connection_fd != 0;
        }

    private:
        int connection_fd;
        mutex* write_mtx;

        string read_chunk() {
            char buf[CHUNK_SIZE+1];
            int n = ::read(connection_fd, buf, CHUNK_SIZE);
            buf[n] = 0;    // null terminating char
            return string(buf);
        }
};

#endif
