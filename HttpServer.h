#ifndef HTTPSERVER_H
#define HTTPSERVER_H

#include <iostream>
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <thread>
using std::thread;
#include <set>
using std::set;

#include "./TcpSocketServer.h"
#include "./TcpSocketConnection.h"
#include "./Request.h"
#include "./Response.h"
#include "./File.h"


set<string> http_versions = {"HTTP/1.0", "HTTP/1.1", "HTTP/2.0"};


class HttpServer: public TcpSocketServer {
    public:
        explicit HttpServer(int port, string _folder): TcpSocketServer(port),
                                                       folder(_folder) {
        }

        void handle_conn(TcpSocketConnection conn) {
            cout << "New connection" << endl;
            vector<thread> request_handlers;

            string line = conn.read_line();
            while (line != "") {
                Request request;
                request.parse_first_line(line);

                // parse header lines
                while (line != "") {
                    line = conn.read_line();
                    request.parse_header_line(line);
                }

                request.info();
                request_handlers.push_back(thread(&HttpServer::handle_request, this, request, conn));

                if (request.version != "HTTP/2.0") {
                    break;
                }
                line = conn.read_line();
            }
            
            for (auto& handler : request_handlers) {
                handler.join();
            }
            cout << "Closing connection" << endl;
            TcpSocketServer::handle_conn(conn);
        }

        void handle_request(Request request, TcpSocketConnection conn) {
            vector<Response> responses;
            if (http_versions.find(request.version) == http_versions.end()) {
                responses.push_back(not_supported_version(request));
            } else if (request.method == "GET") {
                responses = get(request);
            }
            for (auto response : responses) {
                conn.write(response.to_string());
            }
        }

        vector<Response> get(Request request) {
            string orig_path = request.path;
            if (request.path == "/") {
                request.path += "index.html";
            }
            string type = detect_content_type(request.path);

            vector<Response> responses;

            File file(folder + request.path);
            if (!file.good()) {
                responses.push_back(not_found(request));
                return responses;
            }
            string body = file.read_all();

            Response response;
            response.body = body;
            response.version = request.version;
            response.status = "200";
            response.phrase = "OK";
            response.headers["Content-Type"] = type;
            response.headers["Pushed-file"] = orig_path;
            responses.push_back(response);

            if (request.version == "HTTP/2.0" && type == "text/html") {
                vector<string> deps = get_html_dependencies(body);
                for (auto dep : deps) {
                    File file(folder + dep);
                    if (!file.good()) {
                        continue;
                    }

                    Response resp;
                    resp.body = file.read_all();
                    resp.version = request.version;
                    resp.status = "200";
                    resp.phrase = "OK";
                    resp.headers["Content-Type"] = detect_content_type(dep);
                    resp.headers["Pushed-file"] = dep;
                    responses.push_back(resp);
                }
            }

            return responses;
        }

    private:
        string folder;
};

#endif
