CC = g++

all: main.out

main.out: main.cpp
	$(CC) main.cpp -o main.out -std=c++11 -lpthread

clean:
	rm main.out
